<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

$pst_id_v = $_POST['sto_id'];

require_once('global/connection.php');

$query =
"DELETE FROM store
WHERE sto_id = :pst_id_p";

try
{
    $statement = $db->prepare($query);
    $statement->bindParam(':pst_id_p',$pst_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();
    
    //exit($row_count);
    header('Location: index.php');
}

catch(PDOException $e)
{
    $error = $e->getMessage();
    echo $error;
}
?>