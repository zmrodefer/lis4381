<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs

//get item data
$pst_name_v = $_POST['pst_name_js'];
$pst_city_v = $_POST['pst_city_js'];
$pst_state_v = $_POST['pst_state_js'];
$pst_zip_v = $_POST['pst_zip_js'];
$pst_email_v = $_POST['pst_email_js'];
$pst_phone_v = $_POST['pst_phone_js'];
$pst_url_v = $_POST['pst_url_js'];
$pst_ytd_sales_v = $_POST['pst_ytd_sales_js'];
$pst_street_v = $_POST['pst_street_js'];

//variable test
//exit($pst_name_v . "," . $pst_city_v . "," . $pst_email_v . ", etc")

//regexp
//name
$pattern = '/^[\w\-\s]+$/';
$valid_name = preg_match($pattern, $pst_name_v);
//echo $valid_name;
//exit();
//street
$pattern = '/^[a-zA-Z0-9,\s\.]+$/';
$valid_street = preg_match($pattern, $pst_street_v);
//echo $valid_street;
//exit();
//city
$pattern = '/^[\w\s]+$/';
$valid_city = preg_match($pattern, $pst_city_v);
//echo $valid_city;
//exit();
//state
$pattern = '/^[a-zA-Z]+$/';
$valid_state = preg_match($pattern, $pst_state_v);
//echo $valid_state;
//exit();
//zip
$pattern = '/^[0-9]+$/';
$valid_zip = preg_match($pattern, $pst_zip_v);
//echo $valid_zip;
//exit();
//phone
$pattern = '/^[0-9]+$/';
$valid_phone = preg_match($pattern, $pst_phone_v);
//echo $valid_phone;
//exit();
//email
$pattern = '/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $pst_email_v);
//echo $valid_email;
//exit();
//url
$pattern = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url = preg_match($pattern, $pst_url_v);
//echo $valid_url;
//exit();
//ytd_sales
$pattern = '/^[0-9\.]+$/';
$valid_ytd_sales = preg_match($pattern, $pst_ytd_sales_v);
//echo $valid_ytd_sales;
//exit();

if (
    empty($pst_name_v) ||
    empty($pst_street_v) ||
    empty($pst_city_v) ||
    empty($pst_state_v) ||
    empty($pst_zip_v) ||
    empty($pst_phone_v) ||
    empty($pst_email_v) ||
    empty($pst_url_v) ||
    !isset($pst_ytd_sales_v)
)
{
    $error = "all fields require data. Check the fields and try again.";
    include('global/error.php');
}
else if ( !is_numeric($pst_ytd_sales_v) || $pst_ytd_sales_v < 0 ){
    $error = "Error: YTD sales can only contain numbers and a decimal and must be greater than or equal to 0";
}
else if ( $valid_name === 0){
    $error = "Error: name can only contain letters numbers spaces commas hyphens and underscores.";
    include('global/error.php');
}
else if ( $valid_name === false){
    echo 'error in pattern!';
}
else if ( $valid_street === 0){
    $error = "Error: Street can only contain letters numbers spaces commas and periods.";
    include('global/error.php');
}
else if ( $valid_street === false){
    echo 'error in pattern!';
}
else if ( $valid_city === 0){
    $error = "Error: city can only contain letters numbers spaces and hyphens";
    include('global/error.php');
}
else if ( $valid_city === false){
    echo 'error in pattern!';
}
else if ( $valid_state === 0){
    $error = "Error: State can only contain 2 letters";
    include('global/error.php');
}
else if ( $valid_state === false){
    echo 'error in pattern!';
}
else if ( $valid_zip === 0){
    $error = "Error: Zip must contain 5 to 9 numbers only";
    include('global/error.php');
}
else if ( $valid_zip === false){
    echo 'error in pattern!';
}
else if ( $pst_zip_v < 10000 || $pst_zip_v > 999999999){
        $error = "Error: Zip must contain 5 to 9 numbers only";
            include('global/error.php');
}
else if ( $valid_phone === 0){
    $error = "Error: phone can only contain 10 numbers";
    include('global/error.php');
}
else if ( $valid_phone === false){
    echo 'error in pattern!';
}
else if ( $valid_email === 0){
    $error = "Error: Email can only be 100 characters and must follow standard email address syntax";
    include('global/error.php');
}
else if ( $valid_email === false){
    echo 'error in pattern!';
}
else if ( $valid_url === 0){
    $error = "Error: Url must be in standard url format.";
    include('global/error.php');
}
else if ( $valid_url === false){
    echo 'error in pattern!';
}
else if ( $valid_ytd_sales === 0){
    $error = "Error: YTD sales can only contain numbers and decimals";
    include('global/error.php');
}
else if ( $valid_ytd_sales === false){
    echo 'error in pattern!';
}
else {
    require_once('global/connection.php');
    
    $query =
    "INSERT INTO store
    (sto_name, sto_street, sto_city, sto_state, sto_zip, sto_phone, sto_email, sto_ytd_sales, sto_url)
    VALUES
    (:pst_name_p, :pst_street_p, :pst_city_p, :pst_state_p, :pst_zip_p, :pst_phone_p, :pst_email_p, :pst_ytd_sales_p, :pst_url_p)";
    
    try {
        $statement = $db->prepare($query);
        $statement->bindParam(':pst_name_p' , $pst_name_v);
        $statement->bindParam(':pst_street_p' , $pst_street_v);
        $statement->bindParam(':pst_city_p' , $pst_city_v);
        $statement->bindParam(':pst_state_p' , $pst_state_v);
        $statement->bindParam(':pst_zip_p' , $pst_zip_v);
        $statement->bindParam(':pst_phone_p' , $pst_phone_v);
        $statement->bindParam(':pst_email_p' , $pst_email_v);
        $statement->bindParam(':pst_ytd_sales_p' , $pst_ytd_sales_v);
        $statement->bindParam(':pst_url_p' , $pst_url_v);
        $statement->execute();
        $statement->closeCursor();
        $last_auto_increment_id = $db->lastInsertId();
        header('Location: index.php');
        
    }
    catch (PDOException $e){
        $error = $e->getMessage();
        echo $error;
    }
}


//code to process inserts goes here

//include('index.php'); //forwarding is faster, one trip to server
//header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)

?>
