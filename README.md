> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Zach Rodefer


*Assignment 1 Read me*
[Assignment 1 README.md](a1/README.md "Assigntment 1")

    install GIT
    install AMPPS
    install JDK
    install Android studio
    created first application in android studio


*Assignment 2 Read me*
[Assignment 2 README.md](a2/README.md "Assigntment 2")

    Build My Healthy Recips application in android studio

*Assignment 3 Read me*
[Assignment 3 README.md](a3/README.md "Assigntment 3")

    Created and uploaded "pet store" database using mysql workbench


*project 1 Read me*
[Project 1 README.md](p1/README.md "project 1")

    Created and edited XML and Java files in order to customize an Android application interface
    
    
*Assignment 4 Read me*
[Assignment 4 README.md](a4/README.md "Assignment 4")

    Created a client-side validated form for submission into a database using Javascript
    
*Assignment 5 Read me*
[Assignment 5 README.md](a5/README.md "Assignment 5")

    Created a server-side validated form for submission into a database using PHP

*project 2 Read me*
[Project 2 README.md](p2/README.md "project 2")

    AAdded Edit and delete buttons. Added RSS feed
